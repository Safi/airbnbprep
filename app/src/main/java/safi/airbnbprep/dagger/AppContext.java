package safi.airbnbprep.dagger;

import java.lang.annotation.Documented;

import javax.inject.Qualifier;

@Qualifier
@Documented
public @interface AppContext {
}
