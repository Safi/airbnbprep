package safi.airbnbprep.dagger;


import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;
import safi.airbnbprep.MainApplication;

@Singleton
@Component(modules = {AndroidInjectionModule.class, AppModule.class})
public interface AppComponent extends AndroidInjector<MainApplication> {

    @Component.Factory
    interface Factory {
        AppComponent create(@BindsInstance MainApplication application);
    }
}
