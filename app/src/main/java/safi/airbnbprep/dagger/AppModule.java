package safi.airbnbprep.dagger;

import android.app.Application;
import android.content.Context;

import dagger.Binds;
import dagger.Module;
import safi.airbnbprep.MainApplication;
import safi.airbnbprep.api.dagger.ApiModule;
import safi.airbnbprep.model.dagger.ModelModule;
import safi.airbnbprep.ui.fragment.dagger.FragmentsModule;
import safi.airbnbprep.util.executor.dagger.ExecutorModule;
import safi.airbnbprep.util.picasso.dagger.PicassoModule;

@Module(
        includes = {
                ApiModule.class,
                ExecutorModule.class,
                FragmentsModule.class,
                ModelModule.class,
                PicassoModule.class,
        })
abstract class AppModule {

    @Binds
    abstract Application application(MainApplication app);

    @Binds
    @AppContext
    abstract Context applicationContext(MainApplication app);
}
