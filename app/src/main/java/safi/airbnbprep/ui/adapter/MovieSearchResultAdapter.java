package safi.airbnbprep.ui.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import safi.airbnbprep.api.Movie;
import safi.airbnbprep.databinding.ListItemMovieBinding;

public class MovieSearchResultAdapter extends RecyclerView.Adapter<MovieSearchResultAdapter.ViewHolder> {

    private final Picasso mPicasso;

    private List<Movie> mMovies;
    private OnMovieClickListener mListener;

    @Inject
    MovieSearchResultAdapter(Picasso picasso) {
        mPicasso = picasso;
    }

    public void setMovies(List<Movie> movies) {
        mMovies = movies;
    }

    public void setListener(OnMovieClickListener listener) {
        mListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ListItemMovieBinding binding = ListItemMovieBinding.inflate(
                LayoutInflater.from(parent.getContext()),
                parent,
                false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Movie movie = mMovies.get(position);
        holder.mBinding.title.setText(movie.getTitle());
        mPicasso.load(movie.getPosterUrl()).into(holder.mBinding.poster);
        if (mListener != null) {
            holder.itemView.setOnClickListener(v -> mListener.onMovieClick(movie));
        }

    }

    @Override
    public int getItemCount() {
        return mMovies.size();
    }

    public interface OnMovieClickListener {
        void onMovieClick(Movie movie);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        ListItemMovieBinding mBinding;

        ViewHolder(ListItemMovieBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }
}
