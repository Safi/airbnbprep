package safi.airbnbprep.ui.viewmodel;

import android.content.Context;

import java.util.List;

import javax.inject.Inject;

import androidx.annotation.StringRes;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import safi.airbnbprep.R;
import safi.airbnbprep.api.Movie;
import safi.airbnbprep.dagger.AppContext;
import safi.airbnbprep.model.MovieRepository;
import safi.airbnbprep.model.Result;

public class MovieSearchViewModel extends ViewModel {

    private static final int MIN_LENGTH_TITLE = 3;

    private MovieRepository mMovieRepository;
    private Context mContext;

    private final MutableLiveData<Boolean> mSearchResultListVisible = new MutableLiveData<>();
    private final MutableLiveData<Boolean> mErrorMessageVisible = new MutableLiveData<>();
    private final MutableLiveData<String> mErrorMessage = new MutableLiveData<>();
    private final MutableLiveData<List<Movie>> mMovies = new MutableLiveData<>();

    private final Observer<Result<List<Movie>>> mMovieSearchResultObserver = result -> {
        if (result.hasData()) {
            if (result.getData().isEmpty()) {
                onEmpty();
            } else {
                onSuccess(result.getData());
            }
        } else if (result.isError()) {
            onError(R.string.error_message_default);
        }
    };

    private String mCurrentQueryText;
    private LiveData<Result<List<Movie>>> mCurrentMovieSearchResult;

    @Inject
    MovieSearchViewModel(
            @AppContext Context context,
            MovieRepository movieRepository) {
        mContext = context;
        mMovieRepository = movieRepository;
    }

    @Override
    protected void onCleared() {
        maybeRemoveMovieSearchResultObserver();
    }

    private void maybeRemoveMovieSearchResultObserver() {
        if (mCurrentMovieSearchResult != null) {
            mCurrentMovieSearchResult.removeObserver(mMovieSearchResultObserver);
            mCurrentMovieSearchResult = null;
        }
    }

    public LiveData<Boolean> isSearchResultListVisible() {
        return mSearchResultListVisible;
    }

    public LiveData<Boolean> isErrorMessageVisible() {
        return mErrorMessageVisible;
    }

    public LiveData<String> getErrorMessage() {
        return mErrorMessage;
    }

    public LiveData<List<Movie>> getMovies() {
        return mMovies;
    }

    public void searchMovies(String title) {
        title = title.toLowerCase().trim();
        if (title.equals(mCurrentQueryText)) {
            return;
        }
        mCurrentQueryText = title;
        maybeRemoveMovieSearchResultObserver();

        if (title.isEmpty()) {
            onEmpty();
        } else if (title.length() < MIN_LENGTH_TITLE) {
            onError(R.string.error_message_title_too_short);
        } else {
            mCurrentMovieSearchResult = mMovieRepository.searchMovies(title);

            /*
             * It is OK to observe on data from the repository w/ observeForever as this isn't bound to a lifecycle.
             * It observes until told otherwise. The part that you have to handle in that case is to make sure you
             * remove it in onCleared(), which they don't appear to be doing. There's a possibility in this case
             * to leak the ViewModel longer than it should be around since they don't reset() the handler
             * in onCleared().
             */
            mCurrentMovieSearchResult.observeForever(mMovieSearchResultObserver);
        }
    }

    private void onEmpty() {
        mErrorMessageVisible.setValue(false);
        mSearchResultListVisible.setValue(false);
    }

    private void onSuccess(List<Movie> movies) {
        mErrorMessageVisible.setValue(false);
        mSearchResultListVisible.setValue(true);
        mMovies.setValue(movies);
    }

    private void onError(@StringRes int errorMessageId) {
        mErrorMessageVisible.setValue(true);
        mErrorMessage.setValue(mContext.getString(errorMessageId));
        mSearchResultListVisible.setValue(false);
    }
}
