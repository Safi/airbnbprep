package safi.airbnbprep.ui.viewmodel;

import android.content.Context;

import javax.inject.Inject;

import androidx.annotation.StringRes;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import safi.airbnbprep.R;
import safi.airbnbprep.api.MovieDetails;
import safi.airbnbprep.dagger.AppContext;
import safi.airbnbprep.model.MovieRepository;
import safi.airbnbprep.model.Result;

public class MovieDetailsViewModel extends ViewModel {

    private MovieRepository mMovieRepository;
    private Context mContext;

    private final MutableLiveData<Boolean> mMovieDetailsVisible = new MutableLiveData<>();
    private final MutableLiveData<Boolean> mLoadingIndicatorVisible = new MutableLiveData<>();
    private final MutableLiveData<Boolean> mErrorMessageVisible = new MutableLiveData<>();
    private final MutableLiveData<String> mErrorMessage = new MutableLiveData<>();
    private final MutableLiveData<MovieDetails> mMovieDetails = new MutableLiveData<>();

    private final Observer<Result<MovieDetails>> mMovieDetailsResultObserver = result -> {
        if (result.hasData()) {
            onSuccess(result.getData());
        } else if (result.isError()) {
            onError(R.string.error_message_default);
        }
    };

    private String mTitle;
    private LiveData<Result<MovieDetails>> mCurrentMovieDetailsResult;

    @Inject
    MovieDetailsViewModel(
            @AppContext Context context,
            MovieRepository movieRepository) {
        mContext = context;
        mMovieRepository = movieRepository;
    }

    @Override
    protected void onCleared() {
        maybeRemoveMovieSearchResultObserver();
    }

    private void maybeRemoveMovieSearchResultObserver() {
        if (mCurrentMovieDetailsResult != null) {
            mCurrentMovieDetailsResult.removeObserver(mMovieDetailsResultObserver);
            mCurrentMovieDetailsResult = null;
        }
    }

    public LiveData<Boolean> isSearchResultListVisible() {
        return mMovieDetailsVisible;
    }

    public LiveData<Boolean> isErrorMessageVisible() {
        return mErrorMessageVisible;
    }

    public LiveData<String> getErrorMessage() {
        return mErrorMessage;
    }

    public LiveData<Boolean> isLoadingIndicatorVisible() {
        return mLoadingIndicatorVisible;
    }

    public LiveData<MovieDetails> getMovieDetails() {
        return mMovieDetails;
    }

    public void fetchMovieDetails(String title) {
        if (title.equals(mTitle)) {
            return;
        }
        mTitle = title;
        maybeRemoveMovieSearchResultObserver();

        onLoading();
        mCurrentMovieDetailsResult = mMovieRepository.fetchMovieDetails(title);
        mCurrentMovieDetailsResult.observeForever(mMovieDetailsResultObserver);
    }

    private void onLoading() {
        mErrorMessageVisible.setValue(false);
        mMovieDetailsVisible.setValue(false);
        mLoadingIndicatorVisible.setValue(true);
    }

    private void onSuccess(MovieDetails movieDetails) {
        mErrorMessageVisible.setValue(false);
        mLoadingIndicatorVisible.setValue(false);
        mMovieDetailsVisible.setValue(true);
        mMovieDetails.setValue(movieDetails);
    }

    private void onError(@StringRes int errorMessageId) {
        mErrorMessageVisible.setValue(true);
        mErrorMessage.setValue(mContext.getString(errorMessageId));
        mMovieDetailsVisible.setValue(false);
        mLoadingIndicatorVisible.setValue(false);
    }
}
