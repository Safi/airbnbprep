package safi.airbnbprep.ui.view;

import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;

import androidx.annotation.VisibleForTesting;
import androidx.core.util.Preconditions;

public class DebouncedTextWatcher extends EmptyTextWatcher {

    @VisibleForTesting
    static final long DELAY_MS = 500;

    private final Handler mHandler = new Handler(Looper.getMainLooper());
    private final TextWatcher mWrappedWatcher;

    public DebouncedTextWatcher(TextWatcher wrappedWatcher) {
        mWrappedWatcher = Preconditions.checkNotNull(wrappedWatcher);
    }

    @Override
    public void afterTextChanged(Editable s) {
        mHandler.removeCallbacksAndMessages(null);
        mHandler.postDelayed(() -> mWrappedWatcher.afterTextChanged(s), DELAY_MS);
    }
}
