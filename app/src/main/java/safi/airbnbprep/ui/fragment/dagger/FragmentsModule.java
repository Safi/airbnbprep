package safi.airbnbprep.ui.fragment.dagger;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import safi.airbnbprep.ui.fragment.MovieDetailsFragment;
import safi.airbnbprep.ui.fragment.MovieSearchFragment;
import safi.airbnbprep.ui.viewmodel.dagger.ViewModelModule;

@Module(includes = ViewModelModule.class)
public abstract class FragmentsModule {

    @ContributesAndroidInjector
    abstract MovieSearchFragment movieSearchFragment();

    @ContributesAndroidInjector
    abstract MovieDetailsFragment movieDetailsFragment();
}
