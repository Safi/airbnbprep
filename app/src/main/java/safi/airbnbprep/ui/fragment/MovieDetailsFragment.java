package safi.airbnbprep.ui.fragment;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Preconditions;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import dagger.android.support.AndroidSupportInjection;
import safi.airbnbprep.databinding.FragmentMovieDetailsBinding;
import safi.airbnbprep.ui.viewmodel.MovieDetailsViewModel;

public class MovieDetailsFragment extends Fragment {

    @Inject
    ViewModelProvider.Factory mViewModelFactory;

    @Inject
    Picasso mPicasso;

    private MovieDetailsFragmentArgs mArguments;
    private MovieDetailsViewModel mViewModel;
    private FragmentMovieDetailsBinding mViewBinding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel = ViewModelProviders.of(this, mViewModelFactory).get(MovieDetailsViewModel.class);
        mArguments = MovieDetailsFragmentArgs.fromBundle(Preconditions.checkNotNull(getArguments()));
    }

    @Override
    public void onAttach(@NonNull Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mViewBinding = FragmentMovieDetailsBinding.inflate(inflater, container, false);
        initErrorMessageView();
        initLoadingIndicatorView();
        initMovieDetailsViews();
        return mViewBinding.root;
    }

    private void initErrorMessageView() {
        mViewModel
                .isErrorMessageVisible()
                .observe(
                        this,
                        visible -> mViewBinding.errorMessage.setVisibility(visible ? View.VISIBLE : View.INVISIBLE));
        mViewModel
                .getErrorMessage()
                .observe(this, errorMessage -> mViewBinding.errorMessage.setText(errorMessage));
    }

    private void initLoadingIndicatorView() {
        mViewModel
                .isLoadingIndicatorVisible()
                .observe(
                        this,
                        visible -> mViewBinding.loadingIndicator.setVisibility(visible ? View.VISIBLE : View.INVISIBLE));
    }

    private void initMovieDetailsViews() {
        mViewModel.fetchMovieDetails(mArguments.getMovieTitle());

        mViewModel
                .getMovieDetails()
                .observe(
                        this,
                        movieDetails -> {
                            mPicasso.load(movieDetails.getPosterUrl()).into(mViewBinding.poster);
                            mViewBinding.title.setText(movieDetails.getTitle());
                            mViewBinding.year.setText(movieDetails.getYear());
                            mViewBinding.released.setText(movieDetails.getReleased());
                            mViewBinding.runtime.setText(movieDetails.getRuntime());
                            mViewBinding.rated.setText(movieDetails.getRated());
                            mViewBinding.director.setText(movieDetails.getDirector());
                            mViewBinding.actors.setText(movieDetails.getActors());
                            mViewBinding.metaScore.setText(movieDetails.getMetaScore());
                            mViewBinding.plot.setText(movieDetails.getPlot());
                            mViewBinding.textDetailsContainer.requestLayout();
                        });
    }
}
