package safi.airbnbprep.ui.fragment;


import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;
import javax.inject.Provider;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import dagger.android.support.AndroidSupportInjection;
import dagger.android.support.DaggerFragment;
import safi.airbnbprep.api.Movie;
import safi.airbnbprep.databinding.FragmentMovieSearchBinding;
import safi.airbnbprep.ui.adapter.MovieSearchResultAdapter;
import safi.airbnbprep.ui.view.DebouncedTextWatcher;
import safi.airbnbprep.ui.view.EmptyTextWatcher;
import safi.airbnbprep.ui.viewmodel.MovieSearchViewModel;

public class MovieSearchFragment extends DaggerFragment implements MovieSearchResultAdapter.OnMovieClickListener {

    @Inject
    ViewModelProvider.Factory mViewModelFactory;

    @Inject
    Provider<MovieSearchResultAdapter> mAdapterProvider;

    private MovieSearchViewModel mViewModel;
    private FragmentMovieSearchBinding mViewBinding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel = ViewModelProviders.of(this, mViewModelFactory).get(MovieSearchViewModel.class);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mViewBinding = FragmentMovieSearchBinding.inflate(inflater, container, false);
        initQueryTextView();
        initErrorMessageView();
        initResultListView();
        return mViewBinding.root;
    }

    private void initQueryTextView() {
        EmptyTextWatcher textWatcher = new EmptyTextWatcher() {
            @Override
            public void afterTextChanged(Editable text) {
                mViewModel.searchMovies(text.toString());
            }
        };
        mViewBinding.queryText.addTextChangedListener(new DebouncedTextWatcher(textWatcher));
    }

    private void initErrorMessageView() {
        mViewModel
                .isErrorMessageVisible()
                .observe(
                        this,
                        visible -> mViewBinding.errorMessage.setVisibility(visible ? View.VISIBLE : View.INVISIBLE));
        mViewModel
                .getErrorMessage()
                .observe(this, errorMessage -> mViewBinding.errorMessage.setText(errorMessage));
    }

    private void initResultListView() {
        mViewBinding.resultList.setHasFixedSize(true);
        mViewBinding.resultList.setLayoutManager(new LinearLayoutManager(getContext()));
        mViewModel
                .isSearchResultListVisible()
                .observe(
                        this,
                        visible -> mViewBinding.resultList.setVisibility(visible ? View.VISIBLE : View.INVISIBLE));
        mViewModel
                .getMovies()
                .observe(
                        this,
                        movies -> {
                            MovieSearchResultAdapter adapter = getAdapter();
                            adapter.setMovies(movies);
                            mViewBinding.resultList.setAdapter(adapter);
                        });
    }

    private MovieSearchResultAdapter getAdapter() {
        MovieSearchResultAdapter adapter = (MovieSearchResultAdapter) mViewBinding.resultList.getAdapter();
        if (adapter == null) {
            adapter = mAdapterProvider.get();
            adapter.setListener(this);
        }
        return adapter;
    }

    @Override
    public void onMovieClick(Movie movie) {
        MovieSearchFragmentDirections.ActionMovieSearchToMovieDetails action =
                MovieSearchFragmentDirections.actionMovieSearchToMovieDetails(movie.getTitle());
        NavHostFragment.findNavController(this).navigate(action);
    }
}
