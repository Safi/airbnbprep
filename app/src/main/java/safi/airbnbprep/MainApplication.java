package safi.airbnbprep;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import safi.airbnbprep.dagger.DaggerAppComponent;

public class MainApplication extends DaggerApplication {

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.factory().create(this);
    }
}
