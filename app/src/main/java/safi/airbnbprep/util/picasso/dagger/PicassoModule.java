package safi.airbnbprep.util.picasso.dagger;

import android.content.Context;

import com.squareup.picasso.Picasso;

import java.util.concurrent.ExecutorService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import safi.airbnbprep.dagger.AppContext;
import safi.airbnbprep.util.executor.dagger.IO;

@Module
public class PicassoModule {

    @Provides
    @Singleton
    static Picasso providesPicasso(
            @AppContext Context context,
            @IO ExecutorService executorService) {
        return new Picasso
                .Builder(context)
                .executor(executorService)
                .build();
    }
}
