package safi.airbnbprep.util.executor.dagger;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import safi.airbnbprep.util.executor.MainThreadExecutor;

@Module
public class ExecutorModule {

    @Provides
    @Singleton
    @IO
    static ExecutorService providesIOExecutorService() {
        return Executors.newFixedThreadPool(3);
    }

    @Provides
    @Singleton
    @IO
    static Executor providesIOExecutor(@IO ExecutorService executorService) {
        return executorService;
    }

    @Provides
    @Singleton
    @MainThread
    static Executor providesMainThreadExecutor() {
        return new MainThreadExecutor();
    }
}
