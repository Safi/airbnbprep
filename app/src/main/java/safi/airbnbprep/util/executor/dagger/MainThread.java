package safi.airbnbprep.util.executor.dagger;

import java.lang.annotation.Documented;

import javax.inject.Qualifier;

@Qualifier
@Documented
public @interface MainThread {
}
