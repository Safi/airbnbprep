package safi.airbnbprep.model;

public class Result<T> {

    private T mData;
    private boolean mError;
    private String mErrorMessage;

    public Result(T data) {
        this(data, false, null);
    }

    public Result(String errorMessage) {
        this(null, true, errorMessage);
    }

    public Result(T data, boolean error, String errorMessage) {
        mData = data;
        mError = error;
        mErrorMessage = errorMessage;
    }

    public T getData() {
        return mData;
    }

    public boolean isError() {
        return mError;
    }

    public boolean hasData() {
        return mData != null;
    }

    public String getErrorMessage() {
        return mErrorMessage;
    }

    public boolean hasErrorMessage() {
        return mErrorMessage != null;
    }

    public static <T> Result<T> error() {
        return error(null);
    }

    public static <T> Result<T> error(String errorMessage) {
        return new Result<>(errorMessage);
    }

    public static <T> Result<T> data(T data) {
        return new Result<>(data);
    }
}
