package safi.airbnbprep.model;

import java.util.List;

import androidx.lifecycle.LiveData;
import safi.airbnbprep.api.Movie;
import safi.airbnbprep.api.MovieDetails;

public interface MovieRepository {

    LiveData<Result<List<Movie>>> searchMovies(String title);

    LiveData<Result<MovieDetails>> fetchMovieDetails(String title);
}
