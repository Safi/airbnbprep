package safi.airbnbprep.model;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Executor;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import safi.airbnbprep.api.Movie;
import safi.airbnbprep.api.MovieApi;
import safi.airbnbprep.api.MovieDetails;
import safi.airbnbprep.api.MovieSearchResult;
import safi.airbnbprep.util.executor.dagger.IO;

public class DefaultMovieRepository implements MovieRepository {

    private final MovieApi mMovieApi;
    private final Executor mExecutor;

    @Inject
    DefaultMovieRepository(
            MovieApi movieApi,
            @IO Executor executor) {
        mMovieApi = movieApi;
        mExecutor = executor;
    }

    @Override
    public LiveData<Result<List<Movie>>> searchMovies(String title) {
        final MutableLiveData<Result<List<Movie>>> liveData = new MutableLiveData<>();
        mExecutor.execute(() -> {
            try {
                MovieSearchResult movieSearchResult = mMovieApi.searchMovies(title).execute().body();
                if (movieSearchResult.getSearch() != null) {
                    liveData.postValue(Result.data(movieSearchResult.getSearch()));
                } else {
                    liveData.postValue(Result.error(movieSearchResult.getError()));
                }
            } catch (IOException e) {
                liveData.postValue(Result.error(e.getMessage()));
            }
        });
        return liveData;
    }

    @Override
    public LiveData<Result<MovieDetails>> fetchMovieDetails(String title) {
        final MutableLiveData<Result<MovieDetails>> liveData = new MutableLiveData<>();
        mExecutor.execute(() -> {
            try {
                MovieDetails movieDetails = mMovieApi.fetchMovieDetails(title).execute().body();
                if (movieDetails == null) {
                    liveData.postValue(Result.error());
                } else if (Boolean.TRUE.equals(movieDetails.getResponse())) {
                    liveData.postValue(Result.data(movieDetails));
                } else {
                    liveData.postValue(Result.error(movieDetails.getError()));
                }
            } catch (IOException e) {
                liveData.postValue(Result.error(e.getMessage()));
            }
        });
        return liveData;
    }
}
