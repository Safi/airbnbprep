package safi.airbnbprep.model.dagger;

import dagger.Binds;
import dagger.Module;
import safi.airbnbprep.model.DefaultMovieRepository;
import safi.airbnbprep.model.MovieRepository;

@Module
public abstract class ModelModule {

    @Binds
    abstract MovieRepository bindMovieRepository(DefaultMovieRepository repository);
}
