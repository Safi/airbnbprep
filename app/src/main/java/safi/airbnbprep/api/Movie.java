package safi.airbnbprep.api;

import com.google.gson.annotations.SerializedName;

public class Movie {

    @SerializedName("Title")
    private String mTitle;

    @SerializedName("Year")
    private String mYear;

    @SerializedName("Poster")
    private String mPosterUrl;

    public String getTitle() {
        return mTitle;
    }

    public String getYear() {
        return mYear;
    }

    public String getPosterUrl() {
        return mPosterUrl;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "title='" + mTitle + '\'' +
                ", year=" + mYear +
                '}';
    }
}
