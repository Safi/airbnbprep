package safi.airbnbprep.api;

import com.google.gson.annotations.SerializedName;

public class MovieDetails {

    @SerializedName("Response")
    private Boolean mResponse;

    @SerializedName("Error")
    private String mError;

    @SerializedName("Title")
    private String mTitle;

    @SerializedName("Year")
    private String mYear;

    @SerializedName("Rated")
    private String mRated;

    @SerializedName("Released")
    private String mReleased;

    @SerializedName("Runtime")
    private String mRuntime;

    @SerializedName("Director")
    private String mDirector;

    @SerializedName("Actors")
    private String mActors;

    @SerializedName("Plot")
    private String mPlot;

    @SerializedName("Metascore")
    private String mMetaScore;

    @SerializedName("Poster")
    private String mPosterUrl;

    public Boolean getResponse() {
        return mResponse;
    }

    public String getError() {
        return mError;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getYear() {
        return mYear;
    }

    public String getRated() {
        return mRated;
    }

    public String getReleased() {
        return mReleased;
    }

    public String getRuntime() {
        return mRuntime;
    }

    public String getDirector() {
        return mDirector;
    }

    public String getActors() {
        return mActors;
    }

    public String getPlot() {
        return mPlot;
    }

    public String getMetaScore() {
        return mMetaScore;
    }

    public String getPosterUrl() {
        return mPosterUrl;
    }

    @Override
    public String toString() {
        return "MovieDetails{" +
                "title='" + mTitle + '\'' +
                '}';
    }
}
