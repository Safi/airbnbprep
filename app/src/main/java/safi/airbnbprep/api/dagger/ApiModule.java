package safi.airbnbprep.api.dagger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Arrays;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import safi.airbnbprep.api.MovieApi;

@Module
public abstract class ApiModule {

    private static final String BASE_URL = "https://www.omdbapi.com/";

    private static final String VALUE_API_KEY = "7e7c9d33";
    private static final String PARAM_API_KEY = "apikey";

    @Provides
    static MovieApi provideMovieApi() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        OkHttpClient okHttpClient =
                new OkHttpClient()
                        .newBuilder()
                        .addInterceptor(chain -> {
                            Request originalRequest = chain.request();
                            HttpUrl originalUrl = originalRequest.url();

                            HttpUrl url = originalUrl
                                    .newBuilder()
                                    .addQueryParameter(PARAM_API_KEY, VALUE_API_KEY)
                                    .build();

                            Request request = originalRequest
                                    .newBuilder()
                                    .url(url)
                                    .build();

                            return chain.proceed(request);
                        })
                        .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit.create(MovieApi.class);
    }
}
