package safi.airbnbprep.api;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import androidx.annotation.VisibleForTesting;

public class MovieSearchResult {

    @SerializedName("Response")
    private Boolean mResponse;

    @SerializedName("Error")
    private String mError;

    @SerializedName("Search")
    private List<Movie> mSearch;

    @SerializedName("totalResults")
    private Integer mTotalResults;

    public Boolean getResponse() {
        return mResponse;
    }

    public String getError() {
        return mError;
    }

    public List<Movie> getSearch() {
        return mSearch;
    }

    @VisibleForTesting
    public void setSearch(List<Movie> search) {
        mSearch = search;
    }

    public Integer getTotalResults() {
        return mTotalResults;
    }

    @Override
    public String toString() {
        return "MovieSearchResult{" +
                "response=" + mResponse +
                ", result=" + mSearch +
                ", error='" + mError + '\'' +
                ", totalResults=" + mTotalResults +
                '}';
    }
}
