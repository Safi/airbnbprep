package safi.airbnbprep.api;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MovieApi {

    @GET("/")
    Call<MovieSearchResult> searchMovies(@Query("s") String title);

    @GET("/")
    Call<MovieDetails> fetchMovieDetails(@Query("t") String title);
}
