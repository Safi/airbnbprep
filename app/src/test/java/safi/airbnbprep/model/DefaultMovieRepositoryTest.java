package safi.airbnbprep.model;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executor;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.LiveData;
import retrofit2.Call;
import retrofit2.Response;
import safi.airbnbprep.api.Movie;
import safi.airbnbprep.api.MovieApi;
import safi.airbnbprep.api.MovieSearchResult;
import safi.airbnbprep.util.executor.ImmediateExecutor;
import safi.airbnbprep.util.retrofit.FakeCall;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DefaultMovieRepositoryTest {

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    private Executor mExecutor = new ImmediateExecutor();

    @Mock
    private MovieApi mMovieApi;

    private DefaultMovieRepository mSubject;

    @Before
    public void setup() {
        mSubject = new DefaultMovieRepository(mMovieApi, mExecutor);
    }

    @Test
    public void searchMovies_onNetworkError_returnsResultWithError() {
        // setup
        Call<MovieSearchResult> call = FakeCall.failure();
        when(mMovieApi.searchMovies(anyString())).thenReturn(call);

        // act
        LiveData<Result<List<Movie>>> liveData = mSubject.searchMovies("some title");

        //assert
        assertThat(liveData.getValue().isError()).isTrue();
        assertThat(liveData.getValue().hasData()).isFalse();
    }

    @Test
    public void searchMovies_onSuccess_returnsResultWithSearchResult() {
        // setup
        MovieSearchResult searchResult = new MovieSearchResult();
        List<Movie> movieList = Arrays.asList(new Movie(), new Movie());
        searchResult.setSearch(movieList);
        Call<MovieSearchResult> call = FakeCall.response(Response.success(searchResult));
        when(mMovieApi.searchMovies(anyString())).thenReturn(call);

        // act
        LiveData<Result<List<Movie>>> liveData = mSubject.searchMovies("some title");

        //assert
        final Result<List<Movie>> result = liveData.getValue();
        assertThat(result.isError()).isFalse();
        assertThat(result.hasData()).isTrue();
        assertThat(result.getData()).isSameInstanceAs(movieList);
    }
}