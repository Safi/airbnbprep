package safi.airbnbprep.ui.view;

import android.text.SpannableStringBuilder;
import android.text.TextWatcher;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.shadows.ShadowLooper;

import java.util.concurrent.TimeUnit;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(RobolectricTestRunner.class)
public class DebouncedTextWatcherTest {

    private TextWatcher mWrappedListener = mock(TextWatcher.class);
    private DebouncedTextWatcher mSubject = new DebouncedTextWatcher(mWrappedListener);

    @Test
    public void afterTextChanged_firesOnlyOnce() {
        // act
        mSubject.afterTextChanged(new SpannableStringBuilder("a"));
        mSubject.afterTextChanged(new SpannableStringBuilder("ab"));
        mSubject.afterTextChanged(new SpannableStringBuilder("abc"));
        ShadowLooper.shadowMainLooper().idleFor(DebouncedTextWatcher.DELAY_MS, TimeUnit.MILLISECONDS);

        //assert
        verify(mWrappedListener, times(1)).afterTextChanged(any());
    }

    @Test
    public void afterTextChanged_afterDelay_firesAgain() {
        // act
        mSubject.afterTextChanged(new SpannableStringBuilder("a"));
        mSubject.afterTextChanged(new SpannableStringBuilder("ab"));
        mSubject.afterTextChanged(new SpannableStringBuilder("abc"));
        ShadowLooper.shadowMainLooper().idleFor(DebouncedTextWatcher.DELAY_MS, TimeUnit.MILLISECONDS);

        mSubject.afterTextChanged(new SpannableStringBuilder("abcd"));
        mSubject.afterTextChanged(new SpannableStringBuilder("abcde"));
        ShadowLooper.shadowMainLooper().idleFor(DebouncedTextWatcher.DELAY_MS, TimeUnit.MILLISECONDS);

        //assert
        verify(mWrappedListener, times(2)).afterTextChanged(any());
    }
}
