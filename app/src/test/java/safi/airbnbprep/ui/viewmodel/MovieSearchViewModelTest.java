package safi.airbnbprep.ui.viewmodel;

import android.content.Context;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.util.Collections;
import java.util.List;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.MutableLiveData;
import androidx.test.core.app.ApplicationProvider;
import safi.airbnbprep.R;
import safi.airbnbprep.api.Movie;
import safi.airbnbprep.model.MovieRepository;
import safi.airbnbprep.model.Result;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
public class MovieSearchViewModelTest {

    private static final String QUERY_TEXT = "some title";
    private static final String TOO_SHORT_QUERY_TEXT = "a";

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    private Context mAppContext = ApplicationProvider.getApplicationContext();
    private MovieRepository mMovieRepository = mock(MovieRepository.class);
    private MovieSearchViewModel mSubject;

    @Before
    public void setup() {
        mSubject = new MovieSearchViewModel(mAppContext, mMovieRepository);
    }

    @Test
    public void isErrorMessageVisible_onError_returnsTrue() {
        // setup
        MutableLiveData<Result<List<Movie>>> liveData = new MutableLiveData<>();
        liveData.setValue(Result.error());
        when(mMovieRepository.searchMovies(anyString())).thenReturn(liveData);

        // act
        mSubject.searchMovies(QUERY_TEXT);

        //assert
        assertThat(mSubject.isErrorMessageVisible().getValue()).isTrue();
    }

    @Test
    public void isErrorMessageVisible_onTooShortQuery_returnsTrue() {
        // act
        mSubject.searchMovies(TOO_SHORT_QUERY_TEXT);

        //assert
        assertThat(mSubject.isErrorMessageVisible().getValue()).isTrue();
    }

    @Test
    public void getErrorMessage_onTooShortQuery_returnsTooShortMessage() {
        // act
        mSubject.searchMovies(TOO_SHORT_QUERY_TEXT);

        //assert
        assertThat(mSubject.getErrorMessage().getValue())
                .isEqualTo(mAppContext.getString(R.string.error_message_title_too_short));
    }

    @Test
    public void isErrorMessageVisible_onSuccess_returnsFalse() {
        // setup
        MutableLiveData<Result<List<Movie>>> liveData = new MutableLiveData<>();
        liveData.setValue(Result.data(Collections.singletonList(new Movie())));
        when(mMovieRepository.searchMovies(anyString())).thenReturn(liveData);

        // act
        mSubject.searchMovies(QUERY_TEXT);

        //assert
        assertThat(mSubject.isErrorMessageVisible().getValue()).isFalse();
    }

}