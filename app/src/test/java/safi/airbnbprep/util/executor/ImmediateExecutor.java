package safi.airbnbprep.util.executor;

import java.util.concurrent.Executor;

public class ImmediateExecutor implements Executor {

    @Override
    public void execute(Runnable command) {
        command.run();
    }
}
