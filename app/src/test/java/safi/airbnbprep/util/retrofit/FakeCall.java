package safi.airbnbprep.util.retrofit;


import java.io.IOException;

import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FakeCall<T> implements Call<T> {

    private static final IOException EXCEPTION = new IOException("Throwing fake exception to imitate failure");
    private Response<T> mResponse;

    private boolean mExecuted;
    private boolean mCanceled;

    FakeCall(Response<T> response) {
        mResponse = response;
    }

    @Override
    public Response<T> execute() throws IOException {
        mExecuted = true;
        if (mResponse != null) {
            return mResponse;
        }
        throw EXCEPTION;
    }

    @Override
    public void enqueue(Callback<T> callback) {
        mExecuted = true;
        if (mResponse != null) {
            callback.onResponse(this, mResponse);
        } else {
            callback.onFailure(this, EXCEPTION);
        }
    }

    @Override
    public boolean isExecuted() {
        return mExecuted;
    }

    @Override
    public void cancel() {
        mCanceled = true;
    }

    @Override
    public boolean isCanceled() {
        return mCanceled;
    }

    @Override
    public Call<T> clone() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Request request() {
        throw new UnsupportedOperationException();
    }

    public static <T> Call<T> response(Response<T> response) {
        return new FakeCall<>(response);
    }

    public static <T> Call<T> failure() {
        return new FakeCall<>(null);
    }
}
